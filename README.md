# Library System By Group J

Link -  http://groupj.pythonanywhere.com/


### Group J Members
| No. | Name | Gitlab Account |
| -- | -- | -- |
| 1 | Kasasira Charles Derrick | gitlab.com/charleskasasira |
| 2 | Nakuwanda Bridget Hellen | gitlab.com/bridgethellen |
| 3 | Lukyamuzi Akram | gitlab.com/Lukyamuz |
| 4 | Sozzi Henry | gitlab.com/ssozi |

![Homepage](./groupj-library-home.png)

### Technologies used:

- Django
- HTML
- CSS


A web-based library system using django framework that is able to show different books with their details where students can search and request for books which have been posted by the librarian.


### Some of the Challenges faced when implementing the library user stories.

- The steep Django learning curve: We were still understanding the concepts of python as a language and learning Django become a big challenge, but we have hopefully learnt alot from this project.

- The Other different technologies involved: Apart from learning Django. We as a team had to learn git, HTML, CSS and Heroku in order to move this
project to this stage, we later on switched to pythonanywhere for hosting.