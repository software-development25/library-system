# from django.contrib import admin
# from django.contrib import admin
# from .models import Book, BorrowedBook
# import datetime
# from django.utils import timezone

# admin.site.site_header = "Group J Library"

# # Register your models here.

# @admin.register(BorrowedBook)
# class BorrowedBookAdmin(admin.ModelAdmin):
#     list_display=('student','book','issued','returned' ,'days_remaining')
#     list_filter=('issued','returned')
#     fields=('student','book',('issued','returned'),'issued_at','return_date')
#     search_fields=['student__username','book__name']
#     # autocomplete_fields = ['student','book']
#     list_per_page=30

#     def days_remaining(self,obj):
#         if obj.returned:
#             return 'returned'
#         elif obj.return_date :
#             y,m,d=str(timezone.now().date()).split('-')
#             today=datetime.date(int(y),int(m),int(d))
#             y2,m2,d2=str(obj.return_date.date()).split('-')
#             lastdate=datetime.date(int(y2),int(m2),int(d2))
#             if lastdate>today:
#                 return '{} days'.format((lastdate-today).days)
#             return '{} days passed'.format((today-lastdate).days)
#         return 'not issued'
