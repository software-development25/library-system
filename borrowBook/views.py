# from django.shortcuts import render
# from authenticate.models import Book
# from authenticate.forms import BookForm
# from django.contrib.auth.decorators import login_required, user_passes_test
# from django.views.generic import ( 
#   ListView, 
#   DetailView,
# )


# # reserve a book
# @login_required(login_url='login')
# def reserve_book(request, pk):

# 	# context = {'form': form}
# 	book = Book.objects.get(id=pk)
# 	book.status = "reserved"
# 	book.save()
# 	# form = BookForm(instance=book)
# 	if request.method == "POST":
# 		form = BookForm(request.POST, instance=book)
# 		# if form.is_valid:
# 		# 	form.save()
# 		return render(request, 'authenticate/update_book.html', {"book": book, "form": form})

# 	return render(request, 'authenticate/update_book.html', {})



# class ReservedListView(ListView):
#   model = Book 
#   queryset = Book.objects.filter(status='reserved').order_by("-created_at")

# # librarian issues book
# @login_required(login_url='login')
# @user_passes_test(lambda u: u.is_staff, login_url='login') # must be a librarian to issue book
# def issue_book(request, pk):
# 	book = Book.objects.get(id=pk)
# 	book.status = "not available"
# 	book.save()
# 	if request.method == "POST":
# 		form = BookForm(request.POST, instance=book)
# 		# if form.is_valid:
# 		# 	form.save()
# 		return render(request, 'authenticate/update_book.html', {"book": book, "form": form})

# 	return render(request, 'authenticate/update_book.html', {})
