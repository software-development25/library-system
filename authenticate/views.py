from django.shortcuts import render, HttpResponse, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages 
from .forms import SignUpForm, EditProfileForm
from django.contrib.auth.decorators import login_required
from .forms import BookForm
from django.contrib.auth.models import User
from django.views.generic import ( 
  ListView, 
  DetailView,
)
from .models import Book, BorrowedBook

# function to register new student
def register_user(request):
	if request.user.is_anonymous:
		if request.method == 'POST':
			form = SignUpForm(request.POST)
			if form.is_valid():
				form.save()
				username = form.cleaned_data['username']
				password = form.cleaned_data['password1']
				user = authenticate(username=username, password=password)
				login(request, user)
				messages.success(request, ('Welcome: Successful registered!'))
				return redirect('book-list') # on successful signup redirect student to books list.

	else:
		return redirect("book-list")
	form = SignUpForm()
	
	context = {'form': form}
	return render(request, 'authenticate/register.html', context)


# user login
def login_user(request):
	if request.method == 'POST':
		username = request.POST['username']
		password = request.POST['password']
		user = authenticate(request, username=username, password=password)
		if user is not None:
			login(request, user)
			if user.is_staff: # check if user is a staff member then redirect him/her to admin else to the book list.
				return redirect('admin/')
			messages.success(request, ('Successful logged in!'))
			return redirect('book-list')

		else:
			messages.error(request, ('Failed: check your password or username'))
			return redirect('login')
	else:
		return render(request, 'authenticate/login.html', {})


# Books list view
class BookListView(ListView):
  model = Book 
  queryset = Book.objects.all().order_by("-created_at") # sort books by date added in descending order.

# single book detail
class BookDetailView(DetailView):
  model = Book 


#Private Routes: secure views by forcing user to login first using the @login_required decorator.
@login_required(login_url='login') 
def home(request):
	return render(request, 'authenticate/home.html', {})

# add new book to the library
@login_required(login_url='login')
def add_book(request):
	form = BookForm()
	if request.method == 'POST':
		form = BookForm(request.POST, request.FILES)
		if form.is_valid:
			form.save()
			messages.success(request, ('Book was added successfully!'))
			return redirect('book-list')
		else:
			messages.error(request, ('Failed to add book!'))


	context = {'form': form}
	return render(request, 'authenticate/add_book.html', context)

# search for book by title
@login_required(login_url='login')
def search_book(request):
	if request.method == "POST":
		searched = request.POST["searched"]
		books = Book.objects.filter(title__contains=searched)
		return render(request, 'authenticate/search_book.html', {"searched": searched, "books": books})
	else:
		return render(request, 'authenticate/search_book.html', {})

@login_required(login_url='login')
def logout_user(request):
	logout(request)
	messages.success(request, ('Successful logged out!'))
	return redirect('login')


# reserve a book
@login_required(login_url='login')
def reserve_book(request, pk):

	# context = {'form': form}
	book = Book.objects.get(id=pk)
	book.status = "reserved"
	book.save()

	borrow = BorrowedBook.objects.create(book_id=book.id, student_id=request.user.id)
	borrow.save()

	return render(request, 'authenticate/update_book.html', {"book": book})
