from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
import os 
import datetime

# Create your models here.

STATUS_CHOICES = (
  ("available", "available"),
  ("reserved", "reserved"),
  ("not available", "not available")
)

CATEGORY_CHOICES = (
  ("computer", "computer"),
  ("motivation", "motivation"),
  ("biography", "biography"),
  ("history", "history")
)


class Book(models.Model):
  title = models.CharField(max_length=200, unique=False, default="")
  author = models.CharField(max_length=200, null=True)
  status = models.CharField(max_length=200, choices = STATUS_CHOICES, default="available")
  description = models.TextField(null=True)
  cover = models.ImageField(upload_to='uploads/', null=True, blank=True)
  category = models.CharField(max_length=200, choices = CATEGORY_CHOICES, default="computer")
  created_at = models.DateTimeField(default=timezone.now) 

  def __str__(self):
    return self.title

  class Meta: 
    verbose_name_plural = "Books"




# Borrowed books
class BorrowedBook(models.Model):
  student=models.ForeignKey(User, on_delete=models.CASCADE,default=False)
  book=models.ForeignKey(Book,on_delete=models.CASCADE,default=False, db_constraint=False)
  issued=models.BooleanField(default=False)
  issued_at=models.DateTimeField( auto_now=False,null=True,blank=True)
  returned=models.BooleanField(default=False)
  return_date=models.DateTimeField(auto_now=False,auto_created=False,auto_now_add=False,null=True,blank=True)

  def __str__(self):
    return "{}_{} book issue request".format(self.student,self.book)

  def days_no(self):
        "Returns the no. of days before returning / after return_date."
        if self.issued:
            y,m,d=str(timezone.now().date()).split('-')
            today=datetime.date(int(y),int(m),int(d))
            y2,m2,d2=str(self.return_date.date()).split('-')
            lastdate=datetime.date(int(y2),int(m2),int(d2))
            print(lastdate-today,lastdate>today)
            if lastdate > today:
                return "{} left".format(str(lastdate-today).split(',')[0])
            else:
                return "{} passed".format(str(today-lastdate).split(',')[0])
        else:
            return ""

  class Meta: 
      verbose_name_plural = "Borrowed Books"